# Creating User Tokens
See `index.js` to see how to create a user token to secure your SportsTalk 24/7 applications.  

Needed items:
* Your SportsTalk Organization SHARED_SECRET.
* Application IDs if you wish to restrict the user to specific SportsTalk applications.


