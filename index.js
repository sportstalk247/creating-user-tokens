const jwt = require('jsonwebtoken');

function createUserToken(userid, SHARED_SECRET, durationMinutes, applicationIDs) {
    const options = {
        algorithm: 'HS256'
    }
    const payload = {
        sub: userid,
        exp: Math.floor(Date.now() / 1000) + (60 * (durationMinutes || 60)),
    }
    // if you specify application IDs, the JWT will be limited to those applications.
    if(applicationIDs) {
        payload.aud = [].concat(applicationIDs);
    }
    const user_token = jwt.sign(payload, SHARED_SECRET, options);
    return user_token
}

//Example
const token = createUserToken('sampleUser', 'API_SHARED_SECRET', 5);
console.log(token);
console.log(jwt.decode(token));
